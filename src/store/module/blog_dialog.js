export default {
    namespaced: true,
    state: {
        status : false,
        component : '',
        data : '',
        fungsiLoad : '',
        params : {}
    },
    mutations : {
        setStatus: (state, status) => {
            state.status = status
        },
        setComponent: (state, {component, data, params}) => {
            state.component = component
            state.data = data
            state.params = params
        },
    },
    actions: {
        setStatus: ({commit}, {status, component, data, params}) =>{
            commit('setComponent', {component, data, params})
            commit('setStatus', status)
        },
        setComponent: ({commit}, {component, data, params, fungsiLoad}) => {
            commit('setComponent', {component, data, fungsiLoad, params})
            commit('setStatus', true)
        }
    },
    getters: {
        status: state => state.status,
        component: state => state.component,
        data: state => state.data,
        fungsiLoad: state => state.fungsiLoad,
        params: state => state.params
    }
}