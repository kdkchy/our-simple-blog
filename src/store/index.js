import Vue from 'vue'
import Vuex from 'vuex'
import auth_dialog from './module/auth_dialog'
import blog_dialog from './module/blog_dialog'
import auth from './module/auth'
import alert from './module/alert'

import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
    key: 'sanbercode',
    storage: localStorage
})
Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules : {
        auth_dialog,
        blog_dialog,
        auth,
        alert
    }
})