import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: () =>
            import ( /* webpackChunkName: "Home" */ '../views/Home.vue')
    },
    {
        path: '/about',
        name: 'about',
        component: () =>
            import ( /* webpackChunkName: "blog" */ '../views/About.vue')
    },
    {
        path: '/blogs',
        name: 'Blogs',
        component: () =>
            import ( /* webpackChunkName: "blog" */ '../views/blog/Blogs.vue')
    },
    {
        path: '/blog/:id',
        name: 'Blog',
        component: () =>
            import ( /* webpackChunkName: "detailblog" */ '../views/blog/DetailBlog.vue')
    },
    { path: '*', redirect: '/' }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    document.title = 'Our Simple Blog | ' + to.name
    next()
})

export default router