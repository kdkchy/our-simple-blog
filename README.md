## Our Simple Blog
### Akbar Yopiananda    : Layout & Styling
### Esau Batkunda       : CRUD Management
### Kadek Cahya         : Authentication

Screenshot : 
https://drive.google.com/drive/folders/1aHJI-lJiHC6kDiJu6mDVSwK-eZhzwi2k?usp=sharing

Demo : 
https://youtu.be/z7ru5UL1SxI

Deployed :
https://zen-poitras-c5a96a.netlify.app/